const autoprefixer = require("autoprefixer");
const cp = require("child_process");
const cssnano = require("cssnano");
const del = require("del");
const eslint = require("gulp-eslint");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const replace = require('gulp-replace');
const csscomb = require('gulp-csscomb');
const beautify = require('gulp-beautify');

/**
 * Restructure & beauty scss according to .cscomb.json
 */
gulp.task('comb', function () {

    var paths = [
        "scss/**/*.scss",
        "scss/*.scss"
    ];
    return gulp.src(paths, {base: "./"})
        .pipe(csscomb())
        .pipe(beautify.js({indent_size: 2}))
        .pipe(gulp.dest("./"));

});

/**
 * Compile css
 */
function compileCSS() {
    return gulp
        .src("./scss/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(plumber())
        // .pipe(csscomb())
        .pipe(sass({outputStyle: "expanded"}))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("./css/"));
}


gulp.task('css', function () {
    return compileCSS()
});

/**
 * Watch files and compile css on change
 */
function watchFiles() {
    gulp.watch("./scss/**/*", compileCSS);
    gulp.watch("./scss/*", compileCSS);
}

/**
 * Optimize images
 */
gulp.task('imgopt', function () {
    return gulp.src('./img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./img'))
});

/**
 * Prepare for prod deployment
 */
gulp.task('prod', gulp.parallel(
    'imgopt',
    'comb',
    'css'
));


exports.watch = watchFiles;
// exports.prod = prod;