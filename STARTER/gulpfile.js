const autoprefixer = require("autoprefixer");
const cp = require("child_process");
const cssnano = require("cssnano");
const del = require("del");
const eslint = require("gulp-eslint");
const gulp = require("gulp");
const imagemin = require("gulp-imagemin");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const replace = require('gulp-replace');
const csscomb = require('gulp-csscomb');
const beautify = require('gulp-beautify');

/**
 * Restructure & beauty scss according to .cscomb.json
 */
gulp.task('comb', function () {

    var paths = [
        "scss/**/*.scss",
        "scss/*.scss"
    ];
    return gulp.src(paths, { base: "./" })
        .pipe(csscomb())
        .pipe(beautify.js({ indent_size: 2 }))
        .pipe(gulp.dest("./"));

});

/**
 * Compile css
 */
function compileCSS() {
    return gulp
        .src("./scss/*.scss")
        .pipe(sourcemaps.init())
        .pipe(sassGlob())
        .pipe(plumber())
        .pipe(csscomb())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest("./css/"));
}


gulp.task('css', function () {
    return compileCSS()
});

function watchFiles() {
    gulp.watch("./scss/**/*", compileCSS);
    gulp.watch("./scss/*", compileCSS);
}

gulp.task('imgopt', function () {
    return gulp.src('./img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./img'))
});

gulp.task('prod', gulp.parallel(
    'imgopt',
    'comb',
    'css'
));


function directory() {
    return gulp.src('*.*', { read: false })
        .pipe(gulp.dest('./css'))
        .pipe(gulp.dest('./img'))
        .pipe(gulp.dest('./fonts'))
        .pipe(gulp.dest('./js'));
};

gulp.task('setup', function () {

    directory();

    var base = 'technobase';
    var dir = process.cwd().split("/");
    var newname = dir[dir.length - 1];


    //ToDo: Remove old files
    newname = 't2';
    return gulp.src(base + "*")
        .pipe(rename(function (path) {
            var parts = path.basename.split('.');
            if (parts[1]) {
                path.basename = newname + '.' + parts[1];
            } else {
                path.basename = newname;
            }
        }))
        .pipe(replace(base, newname))
        .pipe(replace(base.substr(0, 1).toUpperCase() + base.substr(1), newname.substr(0, 1).toUpperCase() + newname.substr(1)))
        .pipe(del('technobase.info.yml'))
        .pipe(gulp.dest("./"));

    return 0;
});


function delfile() {
    var regexp = /\w*(\-\w{8}\.js){1}$|\w*(\-\w{8}\.css){1}$/;
    gulp.src(['./build/public/train/**/*.js',
        './build/public/train/**/*.css',
        './build/public/train/**/*.scss',
        './build/public/train/**/*.less'
    ]).pipe(deletefile({
        reg: regexp,
        deleteMatch: false
    }))
}

exports.watch = watchFiles;
// exports.prod = prod;