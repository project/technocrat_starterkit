###Compile SASS:

####Install
- npm install

#### Watch
- npm run watch

#### Mixed tasks
- npm run watch  : Watch scss files and compile on change
- npm run comb   : Structure files according to .cscomb.json
- npm run imgopt : Optimize images
- npm run prod   : Optimize images, comb css & compile 


Grid system based on Bootstrap

Breakpoints and how to use them:
xs - Extra small devices - less than 576px
sm - Small devices - 576px and up
md - Medium devices - 768px and up
lg - Large devices - 992px and up
xl - Extra large devices - 1200px and up

Min width

```
@include media-breakpoint-up(sm) {
  // Styles
}
```

Max width

```
@include media-breakpoint-down(sm) {
  // Styles
}
```

Min and max width

```
@include media-breakpoint-between(sm, md) {
  // Styles
}
```
